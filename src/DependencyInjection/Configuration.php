<?php

namespace AzureSpring\Bundle\BincampBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('azurespring_bincamp');

        $rootNode
            ->children()
                ->scalarNode('public_dir')
                    ->defaultValue('public')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
