<?php

namespace AzureSpring\Bundle\BincampBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;

class AzureSpringBincampExtension extends Extension
{
    public function load( array $configs, ContainerBuilder $container )
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration( $configuration, $configs );

        $loader = new Loader\XmlFileLoader( $container, new FileLocator(__DIR__.'/../Resources/config') );
        $loader->load( 'services.xml' );

        $container->setParameter( 'azurespring_bincamp.public_dir', $config['public_dir'] );
    }
}
