<?php

namespace AzureSpring\Bundle\BincampBundle\Twig\Extension;

use AzureSpring\Bincamp\BincampInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class BincampExtension extends AbstractExtension
{
    private $bincamp;


    public function __construct( BincampInterface $bincamp )
    {
        $this->bincamp = $bincamp;
    }

    public function getName()
    {
        return 'azurespring_bincamp';
    }

    public function getFilters()
    {
        return array(
            new TwigFilter(
                'blob',
                function () {
                    $args = func_get_args();

                    $filename = array_shift( $args );
                    if ( !$filename )
                        return null;

                    $argv = array( 0, 0, null, false );
                    foreach ( $argv as &$v ) {
                        $tmp = array_shift( $args );
                        if ( is_null($tmp) )
                            break;

                        if ( is_bool($tmp) ) {
                            $argv[3] = $tmp;
                            break;
                        }

                        $v = $tmp;
                    }

                    list ( $w, $h, $fn, $url ) = $argv;
                    if ( !$fn ) {
                        if ($w > 0 && $h > 0)
                            $fn = 'crop';
                        elseif ($w > 0 || $h > 0)
                            $fn = 'fill';
                    }

                    return $this->bincamp->findURI( $filename,
                                                    $fn ?
                                                    array( $fn, sprintf('%d,%d', $w, $h) ) :
                                                    array() );
                }
            )
        );
    }
}
