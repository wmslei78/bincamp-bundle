<?php

namespace AzureSpring\Bundle\BincampBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use AzureSpring\Bincamp\BincampInterface;
use AzureSpring\Bincamp\ImagickFilter;

class DefaultController
{
    private $bincamp;

    private $payload;

    private $fs;


    public function __construct( BincampInterface $bincamp, $payload, Filesystem $fs )
    {
        $this->bincamp = $bincamp;
        $this->payload = $payload;
        $this->fs      = $fs;
    }

    public function createAction( Request $req )
    {
        return new JsonResponse(array(
            'filenames' => array_map(
                function (UploadedFile $file) {
                    return $this->bincamp->save(
                        $file,
                        $file->guessClientExtension() ?:
                        $file->getClientOriginalExtension() ?:
                        $file->guessExtension()
                    );
                },
                $req->files->get( $this->payload )
            )
        ));
    }

    public function showAction( $loc )
    {
        list( $filename, $cd ) = $this->bincamp->polarize( $loc );
        list( $fn, $sz ) = $cd;
        list( $w, $h ) = explode( ',', $sz );

        $original = $this->bincamp->find( $filename );
        switch ( $fn ) {
            case 'crop':
                $filter = new ImagickFilter\CropFilter( $w, $h );
                break;

            case 'fill':
                $filter = new ImagickFilter\FillFilter( $w, $h, true );
                break;

            default:
                return new BinaryFileResponse( $original );
        }

        $copy = $this->bincamp->find( $filename, $cd );
        $img  = new \Imagick( $original );
        $filter->apply( $img );
        $this->fs->mkdir( dirname($copy) );
        $img->writeImages( $copy, true );

        return new BinaryFileResponse( $copy );
    }
}
