<?php

namespace AzureSpring\Bundle\BincampBundle\Tests;

use AzureSpring\Bundle\BincampBundle\AzureSpringBincampBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AzureSpringBincampBundleTest extends \PHPUnit_Framework_TestCase
{
    public function testInstantiate()
    {
        $bundle = new AzureSpringBincampBundle();

        $this->assertInstanceOf( AzureSpringBincampBundle::class, $bundle );
        $this->assertInstanceOf( Bundle::class, $bundle );
    }
}
