<?php

namespace AzureSpring\Bundle\BincampBundle\Tests\DependencyInjection;

use AzureSpring\Bundle\BincampBundle\DependencyInjection\AzureSpringBincampExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AzureSpringBincampExtensionTest extends \PHPUnit_Framework_TestCase
{

    private $container;

    private $extension;


    public function setUp()
    {
        $this->container = new ContainerBuilder();
        $this->extension = new AzureSpringBincampExtension();
    }

    public function testLoad()
    {
        $this->extension->load([], $this->container);

        $this->assertTrue( $this->container->hasDefinition('azurespring_bincamp.fs') );
        $this->assertTrue( $this->container->hasDefinition('azurespring_bincamp.bincamp_factory') );
        $this->assertTrue( $this->container->hasDefinition('azurespring_bincamp.bincamp') );
        $this->assertTrue( $this->container->hasDefinition('azurespring_bincamp.controller.default') );
        $this->assertTrue( $this->container->hasDefinition('twig.extension.azurespring_bincamp.bincamp') );

        $urlGenerator = $this->container->getDefinition('azurespring_bincamp.bincamp_factory')->getArgument(0);
        $this->assertInstanceOf( Reference::class, $urlGenerator );
        $this->assertEquals( 'router', (string) $urlGenerator );
    }

}
