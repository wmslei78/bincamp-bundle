<?php

namespace AzureSpring\Bundle\BincampBundle\Tests\Twig\Extension;

use AzureSpring\Bundle\BincampBundle\Twig\Extension\BincampExtension;
use AzureSpring\Bincamp\BincampInterface;

class BincampExtensionTest extends \PHPUnit_Framework_TestCase
{
    private $extension;


    public function setUp()
    {
        $this->bincamp =
            $this
            ->getMockBuilder( BincampInterface::class )
            ->disableOriginalConstructor()
            ->getMock()
        ;
        $this->extension = new BincampExtension( $this->bincamp );
    }

    public function testInstantiate()
    {
        $this->assertInstanceOf( BincampExtension::class, $this->extension );
        $this->assertInstanceOf( \Twig_Extension::class, $this->extension );
    }

    public function testBlobFilter()
    {
        foreach ( $this->extension->getFilters() as $f ) {
            if ( $f->getName() == 'blob' )
                break;
        }

        $this->assertEquals( 'blob', $f->getName() );

        $this->bincamp
            ->expects( $this->once() )
            ->method( 'findURI' )
            ->with( $this->equalTo('8b9088ea6518237a9c64712c4db0ee8975dfe14f.jpeg'),
                    $this->equalTo(array( 'crop', '400,300' )) )
            ->willReturn( '/blobs/crop/400,300/8b/90/8b9088ea6518237a9c64712c4db0ee8975dfe14f.jpeg' )
        ;
        $this->assertEquals(
            '/blobs/crop/400,300/8b/90/8b9088ea6518237a9c64712c4db0ee8975dfe14f.jpeg',
            call_user_func( $f->getCallable(),
                            '8b9088ea6518237a9c64712c4db0ee8975dfe14f.jpeg',
                            '400', '300', 'crop' )
        );
    }
}
