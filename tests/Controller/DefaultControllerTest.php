<?php

namespace AzureSpring\Bundle\BincampBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use AzureSpring\Bundle\BincampBundle\Controller\DefaultController;
use AzureSpring\Bincamp\BincampInterface;

class DefaultControllerTest extends \PHPUnit_Framework_TestCase
{
    private $bincamp;

    private $controller;

    private $fs;


    public function setUp()
    {
        $this->bincamp = $this->getMockBuilder( BincampInterface::class )->getMock();
        $this->fs = $this->getMockBuilder( Filesystem::class )->getMock();

        $this->controller = new DefaultController( $this->bincamp, 'payload', $this->fs );
    }

    public function testInstantiate()
    {
        $this->assertInstanceOf( DefaultController::class, $this->controller );
    }

    public function testCreate()
    {
        $payload =
            $this
            ->getMockBuilder( UploadedFile::class )
            ->disableOriginalConstructor()
            ->getMock()
        ;
        $payload
            ->method( 'guessClientExtension' )
            ->willReturn( 'jpeg' )
        ;

        $this->bincamp
            ->method( 'save' )
            ->with( $this->equalTo($payload),
                    $this->equalTo('jpeg') )
            ->willReturn( '500f5b875229e8df3882993130d0d853efabfdb4.jpeg' )
        ;

        $req = $this->getMockBuilder( Request::class )->getMock();
        $req->files = $this->getMockBuilder( FileBag::class )->getMock();
        $req->files
            ->expects( $this->atLeastOnce() )
            ->method( 'get' )
            ->with( $this->equalTo('payload') )
            ->willReturn(array(
                $payload
            ))
        ;

        $this->assertEquals(
            new JsonResponse(array(
                'filenames' => array(
                    '500f5b875229e8df3882993130d0d853efabfdb4.jpeg'
                )
            )),
            $this->controller->createAction( $req )
        );
    }
}
