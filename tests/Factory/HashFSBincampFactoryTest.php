<?php

namespace AzureSpring\Bundle\BincampBundle\Tests\Factory;

use AzureSpring\Bincamp\HashFSBincamp;
use AzureSpring\Bundle\BincampBundle\Factory\HashFSBincampFactory;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class HashFSBincampFactoryTest extends \PHPUnit_Framework_TestCase
{
    private $router;

    private $fs;


    public function setUp()
    {
        $this->router = $this->getMockBuilder( UrlGeneratorInterface::class )->getMock();
        $this->fs = new Filesystem();
        $this->factory = new HashFSBincampFactory( $this->router, $this->fs, '0a656d12-9274-4224-9b89-962e166b34a0' );
    }

    public function testCreateBincamp()
    {
        $this->router
            ->expects( $this->atLeastOnce() )
            ->method( 'generate' )
            ->with( 'azurespring_bincamp_blobs_create' )
            ->willReturn( '/camp' )
            ;

        $bincamp = $this->factory->createHashFSBincamp();
        $this->assertInstanceOf( HashFSBincamp::class, $bincamp );
    }
}
